require "socket"
require "yaml"
require 'logger'

Dir[File.dirname(__FILE__) + '/models/*.rb'].each      {|file| require file }
Dir[File.dirname(__FILE__) + '/controllers/*.rb'].each {|file| require file }

module ToscaCafe
  class Server
    ## ####
    # ### Read config file, set up things, open tcp server
    # ##
    # ↓
    def initialize
      @logger = Logger.new('tosca_cafe.log')
      @logger.info("Starting server")
      config    = YAML.load(IO.read("./config/config.yml"))
      @server   = TCPServer.open( config["ip"], config["port"] )
      @language = config["language"]
      @waiter   = Waiter.new(@language)

      Room.create_default_rooms(config["default_rooms"])
      @logger.info("Default rooms created")
      @current_users = Hash.new
      run
    end

    ## ####
    # ### And here we go, this is where all starts
    # ## Each time a connection comes we do this thing
    # ↓
    def run
      @logger.info("Accepting connections")
      loop {
        begin
          Thread.start(@server.accept) do | client |
            client.set_encoding('UTF-8')
            user = User.new(client, @language)
            welcome_user(user)
            user.tell @waiter.say(:help, user.language)
            @logger.info "New user: #{user.nick_name}"
            listen_user_messages(user)
          end
        rescue IOError => io_error
          # Connection is not possible anymore, close it
          @logger.error("Communication broken: #{io_error}")
          user_lost(user)
        rescue => e
          @logger.error("Something went wrong, error rescued: #{e}")
        end
      }.join
    end

    ## ####
    # ###
    # ## Loop until we got a not taken name
    # ↓
    def welcome_user(user)
      user.tell Art.coffee_cup, true
      user.tell @waiter.say(:welcome, user.language)
      user.tell @waiter.say(:ask_login_name, user.language)
      loop do
        break if nick_name_chosen?(user)
      end
      user.tell ""
      user.tell @waiter.say_welcome_named_user(user.nick_name, user.language)
      @current_users[user.nick_name] = user
    end

    ## ####
    # ### When communication is broken, remove user from the rooms
    # ##  and free the nickname
    # ↓
    def user_lost(user)
      @current_users.delete(user.nick_name)
      ServerCommandController.leave(user, @waiter, true)
    end

    ## ####
    # ### Ask for name and check other nicknames
    # ##
    # ↓
    def nick_name_chosen?(user)
      user.ask_nick_name
      if user.nick_name.match(/(\/|:|@).*/)
        user.tell @waiter.say(:character_not_allowed, user.language)
        return false
      end
      if user.nick_name.to_s.strip == ""
        user.tell @waiter.say(:name_blank, user.language)
        return false
      end
      chosen = true
      @current_users.keys.each do |other_user_nick_name|
        if other_user_nick_name == user.nick_name
          user.tell @waiter.say(:name_taken, user.language)
          user.tell @waiter.say(:ask_login_name, user.language)
          chosen = false
          break
        end
      end
      chosen
    end

    ## ####
    # ### Main iteraction loop: wait for users commands
    # ##
    # ↓
    def listen_user_messages(user)
      loop {
        break unless user.connected?
        begin
          msg = user.get_input
          if msg.match(/^\//)
            process_server_command(user, msg)
          elsif msg.match(/^:/)
            if user.current_room != nil
              process_smiley(user, msg)
            else
              user.tell @waiter.say(:not_room_joined, user.language)
            end
          elsif msg.match(/^@/)
            process_private_message(user, msg)
          else
            if room = user.current_room
              msg = "#{user.nick_name.to_s}: #{msg}"
              room.broadcast_message(msg, user.nick_name)
            else
              user.tell @waiter.say(:not_room_joined, user.language)
            end
          end
        rescue IOError => io_error
          @logger.error("Listen-> Communication broken: #{io_error}")
          user_lost(user)
        rescue => e
          user_lost(user) if msg == nil
          @logger.error("Listen-> Something went wrong, error rescued: #{e}")
        end
      }
    end

    ## ####
    # ### Server commands are processed here
    # ##
    # ↓
    def process_server_command(user, command)
      @logger.info "Processing #{command} (#{user.nick_name})"
      case command
      when "/help"
        ServerCommandController.help(user, @waiter)
      when "/language"
        user.switch_language
        user.tell @waiter.say(:language_set, user.language)
      when "/rooms"
        ServerCommandController.rooms(user, @waiter)
      when /\/join (.+)/
        room_name = command.split('/join ')[1]
        ServerCommandController.join(room_name, user, @waiter)
      when "/leave"
        ServerCommandController.leave(user, @waiter)
      when "/quit"
        @current_users.delete(user.nick_name)
        ServerCommandController.quit(user, @waiter)
      else
        user.tell @waiter.say(:command_not_supported, user.language)
      end
    end

    ## ####
    # ### Smileys!
    # ##
    # ↓
    def process_smiley(user, msg)
      @logger.info "Processing #{msg} (#{user.nick_name})"
      smiley = nil
      case msg
      when ":happy:"
        smiley = Art.happy_guy
      when ":angry:"
        smiley = Art.angry_guy
      when ":wink:"
        smiley = Art.winking_guy
      when ":potato:"
        smiley = Art.mr_potato
      end
      if smiley!=nil
        user.room.broadcast_message("#{user.nick_name}:", user.nick_name)
        user.room.broadcast_message(smiley)
      end
    end

    ## ####
    # ### Private messages between users
    # ##
    # ↓
    def process_private_message(user, msg)
      if user.room == nil
        user.tell @waiter.say(:not_room_joined, user.language)
        return
      end
      to_user_nickname = /@(\S+)/.match(msg)[1]
      private_message  = msg.split(' ', 2)[1]
      if user.nick_name.to_sym == to_user_nickname.to_sym
        user.tell @waiter.say(:private_message_ourself, user.language)
      else
        if user.room.user_exists?(to_user_nickname)
          to_user = user.room.get_user(to_user_nickname)
          to_user.tell @waiter.new_private_message(user.nick_name, to_user.language)
          to_user.tell private_message
        else
          user.tell @waiter.say(:user_doesnt_exist, user.language)
        end
      end
    end
  end
end