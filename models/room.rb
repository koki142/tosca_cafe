
module ToscaCafe
  class Room
    attr_accessor :name, :users

    @@active_rooms = Hash.new
    @@mutex = Mutex.new

    def initialize(room_name)
      @name  = room_name
      @users = Hash.new
    end

    def self.active_rooms
      @@mutex.synchronize { @@active_rooms }
    end

    def self.create_default_rooms(room_names)
      room_names.each{ |room| self.active_rooms[room.to_sym] = Room.new(room) }
    end

    def self.get_listing
      self.active_rooms.values.sort_by {|k| k.users.count }
    end

    def self.get_room(room_name)
      self.active_rooms[room_name.to_sym]
    end

    def get_users
      @@mutex.synchronize { @users }
    end

    def add_user(user)
      get_users[user.nick_name] = user
      user.join_room(self)
    end

    def user_exists?(nick_name)
      get_users[nick_name.to_sym] != nil
    end

    def get_user(nick_name)
      get_users[nick_name.to_sym]
    end

    def remove_user(user)
      get_users.delete(user.nick_name)
    end

    def get_room_info
      "#{@name} (#{get_users.count})"
    end

    def broadcast_message(msg, except_user=nil, with_prefix=false)
      get_users.each do |nick_name, user|
        unless nick_name == except_user
          user.tell(msg, with_prefix)
        end
      end
    end
  end
end