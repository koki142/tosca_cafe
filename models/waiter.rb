module ToscaCafe
  class Waiter
    attr_accessor :language

    def initialize(language)
      @say = {
        en: YAML.load(IO.read("config/en.yml")),
        es: YAML.load(IO.read("config/es.yml"))
      }
      @language = language
      @mutex = Mutex.new
    end

    def get_say
      with_mutex { @say }
    end

    def say(what, lang)
      get_say[lang.to_sym][what.to_s]
    end

    def say_welcome_named_user(nick_name, lang)
      get_say[lang.to_sym]['welcome_named_user'] % {nick_name: nick_name}
    end

    def help_this_guy(user)
      user.tell ""
      user.tell say(:introduction, user.language)
      user.tell ""
      say(:available_commands, user.language).each do |command|
        msg = "/#{command}: "
        msg+= say(:command_explanation, user.language)[command]
        user.tell msg
      end
      user.tell ""
      user.tell say(:smilies, user.language)
      user.tell ""
      user.tell say(:private_message, user.language)
      user.tell ""
    end

    def say_entering_room(room, lang)
      get_say[lang.to_sym]['entering_room'] % {room_name: room}
    end

    def apologize(lang)
      say(:apologies, lang)
    end

    def new_user_joined(room_name, nick_name, lang)
      get_say[lang.to_sym]['new_user_joined'] % {room_name: room_name, nick_name: nick_name}
    end

    def user_left(room_name, nick_name, lang)
      get_say[lang.to_sym]['user_left'] % {room_name: room_name, nick_name: nick_name}
    end

    def private_message_sent(nick_name, lang)
      get_say[lang.to_sym]['private_message_sent'] % {nick_name: nick_name}
    end

    def new_private_message(nick_name, lang)
      get_say[lang.to_sym]['private_message_received'] % {nick_name: nick_name}
    end

    private

    def with_mutex
      @mutex.synchronize { yield }
    end

  end
end