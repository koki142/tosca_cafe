module ToscaCafe
  class User
    SERVER_PREFIX = " * "
    attr_accessor :nick_name, :room, :language

    def initialize(socket, language)
      @nick_name = "guest"
      @socket    = socket
      @room      = nil
      @language  = language
    end

    def connected?
      !@socket.closed?
    end

    def tell(message, prefix=false)
      message = "#{SERVER_PREFIX} #{message}" if prefix
      begin
        @socket.puts message
      rescue
        # Connection down, user out
        if room!=nil
          room.remove_user(self)
          # Todo: this to internationalization
          room.broadcast_message("#{nick_name} disconnected", nil, true)
          leave_room
        end
      end
    end

    def ask_nick_name
      @nick_name = self.get_input.to_sym
    end

    def get_input
      input = @socket.gets.chomp
      if input == "\xFF\xF4\xFF\xFD\u0006" # ^C
        quit
        return nil
      else
        input
      end
    end

    def send_message(msg)
      @socket.puts msg
    end

    def join_room(room)
      @room = room
    end

    def leave_room
      @room = nil
    end

    def current_room
      @room
    end

    def quit
      @socket.close
    end

    def switch_language
      @language = @language == "en" ? "es" : "en"
    end
  end
end