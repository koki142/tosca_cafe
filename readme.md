![Screen Shot 2016-10-26 at 11.51.57.png](https://bitbucket.org/repo/oEyjRb/images/2916814160-Screen%20Shot%202016-10-26%20at%2011.51.57.png)

Running at this moment here: 45.55.164.190:9399
(telnet 45.55.164.190 9399)

Steps to make this work:

- Ruby (comes with OSX)
- Set up config/config.yml:
```
ip: 0.0.0.0
port: 9399
language: en
default_rooms:
  - chat
  - hottub
  - comics and such
  - spain
  - python
  - ruby
```
- From root, run:
```
ruby init.rb
```
- Connect:
```
telnet <ip> <port>
```
- Some logs can be seen in `tosca_cafe.log`

To do someday:

- Communication encryption
- Create rooms (with password?)
- Private rooms with password
- Cool commands such as "it is raining?" (connect to some public api out there)
- Write tests
- Web client
- Database persistence
- Conversation history
- Current user and room status to redis, multiple servers
- Pretty much a slack :)