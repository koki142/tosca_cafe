module ToscaCafe
  class ServerCommandController

    ## ####
    # ### Help command
    # ##
    # ↓
    def self.help(user, waiter)
      waiter.help_this_guy(user)
    end

    ## ####
    # ### Rooms command, show list of rooms ordered by number of users
    # ##
    # ↓
    def self.rooms(user, waiter)
      user.tell waiter.say(:active_rooms, user.language)
      Room.get_listing.each{|room| user.tell(room.get_room_info, true) }
      user.tell waiter.say(:end_of_list, user.language)
    end

    ## ####
    # ### Join command
    # ## - leave current room, if any, and join new one
    # ## - show list of users inside
    # ## - announce new user access to the room
    # ↓
    def self.join(room_name, user, waiter)
      room = Room.get_room(room_name)
      if room
        if user.current_room
          leave(user, waiter)
        end
        user.tell waiter.say_entering_room(room_name, user.language)
        room.add_user(user)
        room.users.values.each do |room_user|
          nick_name = room_user.nick_name
          if nick_name == user.nick_name
            nick_name = "#{nick_name} #{waiter.say(:this_is_you, user.language)}"
          else
            room_user.tell(waiter.new_user_joined(room.name, user.nick_name, room_user.language))
          end
            user.tell nick_name, true
        end
        user.tell waiter.say(:end_of_list, user.language)
      else
        waiter.apologize(user.language)
      end
    end

    ## ####
    # ### Leave command
    # ## - leave current room
    # ## - announce user left to the room
    # ↓
    def self.leave(user, waiter, forced=false)
      if room = user.current_room
        room.remove_user(user)
        user.leave_room
        room.users.values.each do |room_user|
          room_user.tell(waiter.user_left(room.name, user.nick_name, room_user.language))
        end
        nick_name = "#{user.nick_name} #{waiter.say(:this_is_you, user.language)}"
        user.tell(waiter.user_left(room.name, nick_name, user.language)) unless forced
      else
        user.tell waiter.say(:not_room_joined, user.language) unless forced
      end
    end

    ## ####
    # ### Quit command
    # ## - leave current room if any (and announce to the room)
    # ## - close connection
    # ↓
    def self.quit(user, waiter, forced=false)
      leave(user, waiter, forced)
      user.tell(waiter.say(:bye, user.language)) unless forced
      user.quit
    end

  end
end